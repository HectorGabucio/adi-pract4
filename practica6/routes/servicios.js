var models  = require('../models');
var express = require('express');
var router  = express.Router();
var hal = require('hal');
var utils = require('../utils/utils.js'); //definido el login y isEmpty


//estamos en /api/tareas

//caso de uso, listado de TODOS los servicios en la bd
//se devolveran las tareas con los id de los usuarios,
//no devolvemos los objetos usuario porque eso ya lo hacemos con la
//llamada /api/usuarios
router.get('/',utils.login,function(req,res) {
  models.Servicio.findAll({order: 'updatedAt DESC'})
  .then(function(servicios) {
  	//el total de servicios
    var total = servicios.length;
    //si el total es 0, no hay paginacion ni nada
    if(total==0) {
      return res.status(200).send(servicios);
    }
    //en cada pagina tendremos 5 objetos
    var page_size = 5;

    var num_paginas = (total/page_size>>0); //division entera
    var resto =  total % page_size;

    if(resto>0) //si sobran items sin paginar, se añade una ultima pagina para ellos
    	num_paginas++;

    var pagina = '1'; //si no se ha definido el num de pag, por defecto es la pagina 1
    if(req.query.page!=undefined) 
    {
    	pagina=req.query.page;
    }

    var entero_pag = parseInt(pagina);
    if(entero_pag<1 || entero_pag>num_paginas || isNaN(entero_pag)) //paginacion no valida
    {
    	return res.status(400).send("Número de página no válido");
    }

	var link_actual = '/api/servicios';
	if(pagina!='1') //si no es la primera, añadimos query string de page
		link_actual = 'api/servicios?page=' + pagina;

	var link_first = '/api/servicios'; //siempre es la misma
	var link_prev = '/api/servicios';
	if(pagina!='1' && pagina!='2') //si no es la primera ni la segunda, tendra un query string en prev
		link_prev = '/api/servicios?page=' + (entero_pag-1);

	var link_next = '/api/servicios?page=' + num_paginas;
	if(entero_pag<num_paginas) //si es menor que el total de paginas, su next sera pagina+1
		link_next = '/api/servicios?page=' + (entero_pag+1);

	var link_last = '/api/servicios?page=' + num_paginas; //el ultimo link
	if(num_paginas==1) //si solo hay 1 pag, el next y el last son igual que el first
	{
		link_next = link_first;
		link_last = link_first;
	}

	//creando objeto json + paginacion
	var resource = new hal.Resource({},link_actual); //aqui añadimos el link self
	resource.link('first',link_first); //añadimos los demas links
	resource.link('prev',link_prev);
	resource.link('next',link_next);
	resource.link('last',link_last);

	resource.count = page_size; //añadimos el count
	resource.total = total; 	//añadimos el total

	var data = [];

	//configuramos indices de inicio y final
	var it_inicio = (entero_pag-1)*page_size;
	var it_fin = (entero_pag*page_size);
	if(it_fin>total)	//no puede pasarse del total
		it_fin=total;

	for (var i = it_inicio; i < it_fin; i++) { 
    	data.push(servicios[i]);
	}
	resource.data = data; //añadimos el objeto data con los 5 objetos que toquen
	res.status(200).send(resource); //mandamos el objeto json
  });
})





module.exports = router;