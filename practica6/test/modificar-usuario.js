var app = require('../app');
var supertest = require('supertest');
var assert = require('assert');

//para ejecutar los test, usa el comando "npm test" (sin comillas) en la raiz del proyecto, a la altura de package.json
describe('modificar-usuario-tests', function(){
	it('modificar usuario devuelve 204', function(done) {
  	supertest(app)
    	.put("/api/usuarios/3")
    	.set('Authorization','Basic dXN1YXJpbzpwYXNzd29yZA==')
    	.send('nombre=nuevo_usuario_test_modificado')
    	.expect(204)
    	.end(done);

	});

  it('modificar usuario con id no numerico devuelve 400', function(done) {
    supertest(app)
      .put("/api/usuarios/kaaaaa")
      .set('Authorization','Basic dXN1YXJpbzpwYXNzd29yZA==')
      .send('nombre=nuevo_usuario_test_modificado')
      .expect(400)
      .expect(function(respuesta) {
        assert(respuesta.text.indexOf('El id debe ser numérico.')!=-1);
      })
      .end(done);
  });

  it('modificar usuario con id no existente devuelve 404', function(done) {
    supertest(app)
      .put("/api/usuarios/998")
      .set('Authorization','Basic dXN1YXJpbzpwYXNzd29yZA==')
      .send('nombre=nuevo_usuario_test_modificado')
      .expect(404)
      .expect(function(respuesta) {
        assert(respuesta.text.indexOf('El recurso con dicho id no existe.')!=-1);
      })
      .end(done);
  });

  it('modificar usuario sin body nombre devuelve 400', function(done) {
    supertest(app)
      .put("/api/usuarios/3")
      .set('Authorization','Basic dXN1YXJpbzpwYXNzd29yZA==')
      .expect(400)
      .expect(function(respuesta) {
        assert(respuesta.text.indexOf('Falta el campo nombre, o su valor está vacío.')!=-1);
      })
      .end(done);
  });


})