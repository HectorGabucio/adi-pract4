var app = require('../app');
var supertest = require('supertest');
var assert = require('assert');

//para ejecutar los test, usa el comando "npm test" (sin comillas) en la raiz del proyecto, a la altura de package.json
describe('crear-servicio-usuario-tests', function(){
	it('crear servicio devuelve 201', function(done) {
    	supertest(app)
      	.post("/api/usuarios/3/servicios")
      	.set('Authorization','Basic dXN1YXJpbzpwYXNzd29yZA==')
      	.send('titulo=nuevo_servicio_test')
      	.expect(201)
      	.end(done);
	});

  it('crear servicios para users con id no numerico devuelve 400', function(done) {
    supertest(app)
    .post("/api/usuarios/-bla/servicios")
    .set('Authorization','Basic dXN1YXJpbzpwYXNzd29yZA==')
    .send('titulo=nuevo_servicio_test')
    .expect(400)
    .expect(function(respuesta) {
      assert(respuesta.text.indexOf("El id debe ser numérico.")!=-1);
    })
    .end(done);
  });

  it('crear servicios para users sin body titulo devuelve 400', function(done) {
    supertest(app)
    .post("/api/usuarios/3/servicios")
    .set('Authorization','Basic dXN1YXJpbzpwYXNzd29yZA==')
    .expect(400)
    .expect(function(respuesta) {
      assert(respuesta.text.indexOf("Falta el campo titulo, o su valor está vacío.")!=-1);
    })
    .end(done);
  });

  it('crear servicios para usuario con id no existente devuelve 404', function(done) {
    supertest(app)
    .post('/api/usuarios/99/servicios')
    .set('Authorization','Basic dXN1YXJpbzpwYXNzd29yZA==')
    .send('titulo=nuevo_servicio_test')
    .expect(404)
    .expect(function(respuesta) {
      assert(respuesta.text.indexOf("El recurso con dicho id no existe.")!=-1);
    })
    .end(done);
  });


})