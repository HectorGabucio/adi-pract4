var app = require('../app');
var supertest = require('supertest');
var assert = require('assert');

//para ejecutar los test, usa el comando "npm test" (sin comillas) en la raiz del proyecto, a la altura de package.json
describe('crear-usuario-tests', function(){
	it('crear usuario devuelve 201', function(done) {
    	supertest(app)
      	.post("/api/usuarios")
      	.set('Authorization','Basic dXN1YXJpbzpwYXNzd29yZA==')
      	.send('nombre=nuevo_usuario_test')
      	.expect(201)
      	.expect(function(respuesta) {
			assert(respuesta.text.indexOf('"nombre":"nuevo_usuario_test"')!=-1);
		})
      	.end(done);

	});

	it('crear usuario sin nombre devuelve 400', function(done) {
    	supertest(app)
      	.post("/api/usuarios")
      	.set('Authorization','Basic dXN1YXJpbzpwYXNzd29yZA==')
      	.expect(400)
      	.expect(function(respuesta) {
			assert(respuesta.text.indexOf('Falta el campo nombre, o su valor está vacío.')!=-1);
		})
      	.end(done);

	});

})