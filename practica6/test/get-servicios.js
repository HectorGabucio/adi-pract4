var app = require('../app');
var supertest = require('supertest');
var assert = require('assert');

//para ejecutar los test, usa el comando "npm test" (sin comillas) en la raiz del proyecto, a la altura de package.json
describe('get servicios-tests', function(){
	it('get servicios devuelve los servicios', function(done) {
		supertest(app)
		.get('/api/servicios')
		.set('Authorization','Basic dXN1YXJpbzpwYXNzd29yZA==')
		.expect(200)
		.expect(function(respuesta) {
			assert(respuesta.text.indexOf('{"_links":{"self":{"href":"/api/servicios"}')!=-1);
		})
		.end(done);
	});

	it('get servicios con credenciales malas devuelve 403',function(done) {
		supertest(app)
		.get('/api/servicios')
		.set('Authorization','Basic dXN1YXJpb')
		.expect(403)
		.expect(function(respuesta) {
			assert(respuesta.text.indexOf("Las credenciales introducidas son incorrectas")!=-1);
		})
		.end(done);
	});

	it('get servicios con credenciales inexistentes devuelve 401',function(done) {
		supertest(app)
		.get('/api/servicios')
		.expect(401)
		.expect('WWW-Authenticate','Basic realm="Mi realm"')
		.expect(function(respuesta) {
			assert(respuesta.text.indexOf("No has introducido tus credenciales")!=-1);
		})
		.end(done);
	});

	it('get servicios con pagina no valida devuelve 400',function(done) {
		supertest(app)
		.get('/api/servicios?page=-1')
		.set('Authorization','Basic dXN1YXJpbzpwYXNzd29yZA==')
		.expect(400)
		.expect(function(respuesta) {
			assert(respuesta.text.indexOf("Número de página no válido")!=-1);
		})
		.end(done);
	});


})