var appServices = angular.module('appServices', []);


//servicio para guardar un login que no es de modal
appServices.service('loginSvc',function() {
  return function(user) {
    localStorage.setItem('user', JSON.stringify(user));
  }
})

//servicio para crear un modal de login
appServices.service('loginModal', function ($uibModal) {
  //funcion para guardar el usuario en el local storage
  function guardaUsuario (user) {
    localStorage.setItem('user', JSON.stringify(user));
  }

  //este servicio devuelve una funcion (sin ejecutar).
  //la funcion abrira un nuevo modal que pedira las credenciales
  //a los usuarios,
  //modal.open es una promise,que se puede cancelar cerrando
  //la ventana
  return function() {
    var instance = $uibModal.open({
      templateUrl: 'aplicacion/templates/login-modal.html',
      controller: 'LoginModalCtrl',
      controllerAs: 'LoginModalCtrl',
      windowClass: 'app-modal-window'
    })

    return instance.result.then(guardaUsuario);
  };
});

//servicio factoria API
appServices.factory('API',
  function($http) {

    //funcion auxiliar para conseguir la cadena de autorizacion basic
    function getCredenciales() {
      var credenciales = "INCORRECTAS";
      if(localStorage.getItem('user')) {
        credenciales = JSON.parse(localStorage.getItem('user'));

        //Esto lo hago porque en la API, las credenciales son siempre usuario:password
        //en la aplicacion web necesito guardar en el localstorage un nombre de usuario
        //existente en la BD (Paco, Héctor...)
        credenciales.login='usuario';



        
        credenciales = credenciales.login + ':' + credenciales.password;
        //aqui ya tenemos la cadena en basic64 preparada
        credenciales = 'Basic ' + btoa(credenciales);
      }
      return credenciales;
    };

    //las funciones de la factoria
    return {

      //devuelve coleccion de servicios, paginado
      getListaServicios: function(num_pagina) {
        var url = '/api/servicios';
        if(num_pagina>1)
          url = url + '?page=' + num_pagina;
        return $http.get(url, {
          headers: {'Authorization': getCredenciales()}
        }).then(function(result) {
          //esto es raro, al usar then en vez de succes, hay que enviar
          //result.data en vez de result, ya que then incluye metadatos.
          return result.data;
        });
      },

      //con el localstorage sabra que usuario es
      getMisServicios: function(num_pagina) {
        var user_id = JSON.parse(localStorage.getItem('user')).id;
        var url = '/api/usuarios/' + user_id + '/servicios';
        if(num_pagina>1)
          url = url + '?page=' + num_pagina;
        return $http.get(url, {
          headers: {'Authorization': getCredenciales()}
        }).then(function(result) {
          //esto es raro, al usar then en vez de succes, hay que enviar
          //result.data en vez de result, ya que then incluye metadatos.
          return result.data;
        });
      },

      crearServicio: function(datos) {
        var data = {};
        data.titulo = datos.titulo;
        //data.user_id = datos.usuario;
        return $http.post(
          '/api/usuarios/' + datos.usuario + '/servicios',
          data,
          {
            headers: {'Authorization': getCredenciales()}
          }
        );
      },

      modificarServicio: function(service_id,nuevo_titulo) {
        var data = {};
        data.titulo = nuevo_titulo;
        var user_id = JSON.parse(localStorage.getItem('user')).id;
        var url = '/api/usuarios/' + user_id + '/servicios/' + service_id;
        return $http.put(
          url,
          data,
          {
            headers: {'Authorization': getCredenciales()}
          }
        );
      },

      borrarServicio: function(service_id) {
        var user_id = JSON.parse(localStorage.getItem('user')).id;
        var url = '/api/usuarios/' + user_id + '/servicios/' + service_id;
        return $http.delete(url, {
          headers: {'Authorization': getCredenciales()}
        });

      },

      borrarUsuarioLogeado: function() {
        var id = JSON.parse(localStorage.getItem('user')).id;
        var url = '/api/usuarios/' + id;
        return $http.delete(url, {
          headers: {'Authorization': getCredenciales()}
        }).then(function(result) {
          localStorage.clear(); //cerramos sesion
          return result;
        })
      },

      modificarUsuarioLogeado: function(nuevos_datos) {
        var id = JSON.parse(localStorage.getItem('user')).id;
        var url = '/api/usuarios/' + id;
        var data = {};

        //el nombre obligatorio que no este vacio
        data.nombre = nuevos_datos.nuevo_nombre;

        //opcionales
        data.genero = nuevos_datos.nuevo_genero;
        data.email = nuevos_datos.nuevo_email;
        data.telefono = nuevos_datos.nuevo_telefono;

        return $http.put(url,data, {
          headers: {'Authorization': getCredenciales()}
        }).then(function(result) {
          var user = JSON.parse(localStorage.getItem('user'));
          user.id = id;
          user.login=data.nombre;
          localStorage.setItem('user',JSON.stringify(user));
          return result;
        })
      },

      //devuelve coleccion de usuarios, paginado
      getListaUsuarios: function(num_pagina) {
        var url = '/api/usuarios';
        if(num_pagina>1)
          url = url + '?page=' + num_pagina;
        return $http.get(url, {
          headers: {'Authorization': getCredenciales()}
        }).then(function(result) {
          return result.data;
        });
      },

      getUsuario: function(user_id) {
        var id = user_id;
        var url = '/api/usuarios/' + id;
        return $http.get(url, {
          headers: {'Authorization': getCredenciales()}
        });
      },

      //devuelve 200 si el login es valido, 403 si no
      login: function(username, password) {
        var data = {};
        data.login = username;
        data.password = password;
        return $http.post('/api/login', data);
      }


    };
  }
);