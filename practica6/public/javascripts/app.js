var MiApp = angular.module('miApp', [
  'appControllers',
  'appServices'
]);




$(document).ready(function() {
localStorage.clear(); //al recargar pagina volvemos al principio
$.mobile.pageContainer.pagecontainer("change", "#loginpage");

});

MiApp.directive('myRepeatDirective', function() {
  return function(scope, element, attrs) {
    if (scope.$last){ //cuando el ultimo elemento este cargado...
      
      //LISTA DE SERVICIOS
      if($('#nuevos-servicios').hasClass('ui-listview'))
        $('#nuevos-servicios').listview('refresh');
      else
        $('#nuevos-servicios').trigger('create');

      /*
      //MIS-SERVICIOS
      if($('#mis-servicios').hasClass('ui-listview'))
        $('#mis-servicios').listview('refresh');
      else
        $('#mis-servicios').trigger('create');

      //LISTA-USUARIOS
      if($('#lista-usuarios').hasClass('ui-listview'))
        $('#lista-usuarios').listview('refresh');
      else
        $('#lista-usuarios').trigger('create');
      */

    }
  };
});
