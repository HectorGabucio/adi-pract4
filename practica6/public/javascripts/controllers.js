var appControllers = angular.module('appControllers', ['appServices']);


//para mostrar toasts
var cuadro_mensaje = function(mensaje,esError) {

  var color = "";
  var bordercolor="";
  var icono = "";
  if(esError)
  {
    //color="red";
    color="#d9534f";
    icono = '<i class="zmdi zmdi-alert-circle-o zmd-2x"></i>';
    bordercolor = "#d43f3a";
  }

  else
  {
    color="#449d44";
    icono = '<i class="zmdi zmdi-check-circle-u zmd-2x"></i>';
    bordercolor = "#398439";
  }

  new $.nd2Toast({
    message : icono + '<strong> ' + mensaje + '</strong>',
    ttl : 3000 //tiempo de vida
  });
  $( ".nd2-toast" ).css('background-color',color); //o nd2-toast-wrapper
  $( ".nd2-toast" ).css('border','2px solid ' + bordercolor); //o nd2-toast-wrapper
}



var formatear_fecha = function(fentrada) {
  var datos_fecha = fentrada.split("T");
  var nueva_fecha = datos_fecha[0].split('-').join(' ');

  var fecha = moment(nueva_fecha, "YYYY MM DD","es");
  var hora = datos_fecha[1].split(".")[0];
  var array_tiempo = hora.split(":");

  return fecha.format("LL") + " a las " + array_tiempo[0] +":" + array_tiempo[1] ;
}


//controlador para los datos del header
appControllers.controller('HeaderCtrl',['$rootScope',
  function($rootScope) {

    $rootScope.recargarHeader = function() {
      $rootScope.titulo = 'No has iniciado sesión todavía';
      if(localStorage.getItem('user'))
      {
          $rootScope.titulo = "Hola " + JSON.parse(localStorage.getItem('user')).login;
      }

      $rootScope.logout = function() {
        localStorage.clear();
        $.mobile.pageContainer.pagecontainer("change", "#loginpage", { transition: 'flow'});

        
      };
    };

    //cada vez que se cambie de pagina, se recargara la header
    $(document).on('pagebeforeshow', function() {
      $rootScope.recargarHeader();
    });
  }]
);












//Controlador del login
appControllers.controller('LoginCtrl',['$scope','API','loginSvc',
  function ($scope,API,loginSvc) {
    $scope.submit = function (username, password) {
      API.login(username, password).then(
        function(result) { //callback resultado
          //scope tiene close ya que es un modal
          //se devuelve el usuario que es correcto
          var user ={};
          user.id=result.data[0].id;
          user.login=username;
          user.password=password;
          //aqui mandamos al service los datos del usuario
          loginSvc(user);
          cuadro_mensaje("Has iniciado sesión correctamente",false);
          $.mobile.pageContainer.pagecontainer(
            "change", "#listaServiciosPage", { transition: 'flow'});

        },
        function(error) { //callback error
          cuadro_mensaje(error.data,true);
        });
    };
  }]
);


//Controlador de registro
appControllers.controller('RegisterCtrl',['$scope','API','loginSvc',
  function ($scope,API,loginSvc) {

    $scope.crearUsuario = function() {
      var data = {};
      data.nombre = document.getElementById("nombre").value;
      data.genero = $('input[name=generoreg]:checked').val();
      data.email = document.getElementById("email").value;
      data.telefono = document.getElementById("telefono").value;
      
      $.ajax({
        url: '/api/usuarios/',
        data: data,
        async: true,
        type: 'POST',
        success: function(results) {
          cuadro_mensaje("Te has registrado correctamente.",false);
          //guardamos en localstorage
          var user ={};
          user.id=results.id;
          user.login=results.nombre;
          user.password='password';
          localStorage.setItem('user', JSON.stringify(user));
          $.mobile.pageContainer.pagecontainer("change", "#listaServiciosPage", { transition: 'flow'});
        },
        error: function(results) {
          cuadro_mensaje(results.responseText,true);
          //alert("error")
        }
      });
    }
  }]
);

//Controlador para la lista de servicios
appControllers.controller('listaServiciosCtrl',['$scope','$rootScope','API',
  function ($scope,$rootScope,API) {
    $scope.recargarLista = function() {

      //El titulo (que sale en la pestaña y eso)
      $(document).ready(function ()
      {
        document.title = "Lista de servicios";
      });



      API.getListaServicios(1).then(function(result) {
        $scope.puedo_modificar=false;
        $scope.services = result.data;

        //formatear fechas
        for(var i=0;i<result.data.length;i++) {
         result.data[i].updatedAt=formatear_fecha(result.data[i].updatedAt);
        }


        $scope.num_pag = 1; //la pagina inicial es la 1
        //esta funcion actualiza los datos con pagina nueva
        $scope.page = function(numero) {
          var num_minimo = 1;
          //division entera
          var num_maximo = (result.total/result.count>>0);
          if(result.total%result.count>0) //si sobra algun elemento, crear una pagina adicional
            num_maximo+=1;
          //que el numero de pagina sea valido
          if(numero>num_maximo)
            numero=num_maximo;
          if(numero<num_minimo)
            numero=num_minimo;
          //llamada asincrona para cambiar de pagina
          API.getListaServicios(numero).then(
            function(resultados) { //callback success
              $scope.services = resultados.data;
              //formatear fechas
              for(var i=0;i<resultados.data.length;i++) {
               resultados.data[i].updatedAt=formatear_fecha(resultados.data[i].updatedAt);
              }
              $scope.num_pag = numero;
            },
            function(error) { //calback error
              cuadro_mensaje("Fallo de la base de datos o el servidor",true);
            });
        }

        $scope.detalle = function(servicio) {
          $.mobile.pageContainer.pagecontainer(
            "change", "#detalleServicioPage",{servicio: servicio});
       
        }

      });
    }

    //cada vez que se entre en la pagina de listaservicios, se recarga
    $('#listaServiciosPage').on('pagebeforeshow', function() {
      $scope.recargarLista();
    });

  }]
);




//Controlador para mis servicios
appControllers.controller('misServiciosCtrl',['$scope','$rootScope','API',
  function ($scope,$rootScope,API) {
    $scope.inicializarControlador = function() {
      //El titulo (que sale en la pestaña y eso)
      $(document).ready(function ()
      {
        document.title = "Mis servicios";
      });

      API.getMisServicios(1).then(function(result) {
        $scope.puedo_modificar=false;
        $scope.services = result.data;
        //formatear fechas
        for(var i=0;i<result.data.length;i++) {
         result.data[i].updatedAt=formatear_fecha(result.data[i].updatedAt);
        }

        $scope.num_pag = 1; //la pagina inicial es la 1
        //esta funcion actualiza los datos con pagina nueva
        $scope.page = function(numero) {
          var num_minimo = 1;
          //division entera
          var num_maximo = (result.total/result.count>>0);
          if(result.total%result.count>0) //si sobra algun elemento, crear una pagina adicional
            num_maximo+=1;
          //que el numero de pagina sea valido
          if(numero>num_maximo)
            numero=num_maximo;
          if(numero<num_minimo)
            numero=num_minimo;
          //llamada asincrona para cambiar de pagina
          API.getMisServicios(numero).then(
            function(resultados) { //callback success
              $scope.services = resultados.data;

              for(var i=0;i<resultados.data.length;i++) {
               resultados.data[i].updatedAt=formatear_fecha(resultados.data[i].updatedAt);
              }


              $scope.num_pag = numero;
            },
            function(error) { //calback error
              cuadro_mensaje("Fallo de la base de datos o el servidor",true);
            });
        }

        $scope.borrar = function(servicioid) {
          API.borrarServicio(servicioid).then(function(result) 
          {
            cuadro_mensaje("¡Servicio borrado!",false);
            actualizar($scope.num_pag);
          })
        };

        $scope.editar = function(servicio) {
          $.mobile.pageContainer.pagecontainer(
            "change", "#editarServicioPage",{servicio: servicio});
        }


        var actualizar = function(num_pag) {
          $scope.num_pag = num_pag; //la pagina inicial es la 1
          API.getMisServicios($scope.num_pag).then(
          function(result) 
          {
            $scope.services = result.data;
            //esta funcion actualiza los datos con pagina nueva
            $scope.page = function(numero) {
              $scope.activeServiceIndex=-1; //no tiene que haber ningun form desplegado
              var num_minimo = 1;
              //division entera
              var num_maximo = (result.total/result.count>>0);
              if(result.total%result.count>0) //si sobra algun elemento, crear una pagina adicional
                num_maximo+=1;
              //que el numero de pagina sea valido
              if(numero>num_maximo)
                numero=num_maximo;
              if(numero<num_minimo)
                numero=num_minimo;
              //llamada asincrona para cambiar de pagina
              API.getMisServicios(numero).then(
                function(resultados) { //callback success
                  $scope.services = resultados.data;
                  $scope.num_pag = numero;
                },
                function(error) { //calback error
                  //cuadro_mensaje("Fallo de la base de datos o el servidor",true);
                }
              );
            };
          },
          function(error) { //si en una pagina no hay objetos, a la anterior
            actualizar(num_pag-1);
          });
        };



      });
    }

    //cada vez que se entre en la pagina de listaservicios, se recarga
    $('#misServiciosPage').on('pagebeforeshow', function() {
      $scope.inicializarControlador();
    });

  }]
);









//Controlador para el detalle de servicio
appControllers.controller('detalleServicioCtrl',['$scope','API',
  function ($scope,API) {
    $scope.servicio={};
    //conseguir el objeto que le hemos pasado con el pagecontainer
    $(document).on("pagebeforechange", function (e, data) {
      if (data.toPage[0].id == "detalleServicioPage") {
          var servicio = data.options.servicio;
          if(servicio!=undefined)
          {

            API.getUsuario(servicio.UsuarioId).then(function(result) {
              //metemos tambien datos basicos del usuario
              servicio.usuario = result.data[0]
            });

            servicio.createdAt = formatear_fecha(servicio.createdAt);
            $scope.servicio=servicio;
          }
      }
    });
  }]
);



//Controlador para crear servicio
appControllers.controller('crearServicioCtrl',['$scope','API',
  function ($scope,API) {
    $scope.usuario={};
    $scope.inicializarControlador = function() {
       var user = JSON.parse(localStorage.getItem('user'));
    };

    $scope.crear = function(titulo) {
      if(titulo!=undefined && titulo!="") //si hay texto
      {
        var datos = {};
        datos.titulo = titulo;
        datos.usuario = JSON.parse(localStorage.getItem('user')).id;

        API.crearServicio(datos)
        .then(function(result) {
          cuadro_mensaje("¡Servicio creado!",false);
          $.mobile.pageContainer.pagecontainer(
            "change", "#misServiciosPage", { transition: 'flow'});
        })
        .catch(function(error) {
          cuadro_mensaje(error.data,true);
        });
        
      }
      else
      {
        cuadro_mensaje("El campo no puede estar vacío...",true);
      }
    }

    //cada vez que se entre en la pagina de nuevo servicio, se recarga
    $('#nuevoServicioPage').on('pagebeforeshow', function() {
      $scope.inicializarControlador();
    });
  }]
);




//Controlador para editar servicio
appControllers.controller('editarServicioCtrl',['$scope','API',
  function ($scope,API) {
    $scope.servicio={};
    //conseguir el objeto que le hemos pasado con el pagecontainer
    $(document).on("pagebeforechange", function (e, data) {
      if (data.toPage[0].id == "editarServicioPage") {
        var servicio = data.options.servicio;
        var servicio = data.options.servicio;
        if(servicio!=undefined) //de las 2 callbacks, que sea la segunda
        {
          $scope.servicio = servicio;
          $scope.modificar = function() {
            var titulo = $scope.servicio.titulo;
            if(titulo!=undefined && titulo!="")
            {
              API.modificarServicio($scope.servicio.id,titulo)
              .then(function(result) {
                cuadro_mensaje("¡Servicio modificado!",false);
                $.mobile.pageContainer.pagecontainer(
                  "change", "#misServiciosPage", { transition: 'flow'});
              })
              .catch(function(error) {
                cuadro_mensaje(error.data,true);
              });
            }
            else
            {
              cuadro_mensaje("El campo no puede estar vacío...",true);
            }
          };
        }

      }
    });
  }]
);




//Controlador para la lista de usuarios
appControllers.controller('listaUsuariosCtrl',['$scope','$rootScope','API',
  function ($scope,$rootScope,API) {
    $scope.recargarLista = function() {
      //El titulo (que sale en la pestaña y eso)
      $(document).ready(function ()
      {
        document.title = "Lista de usuarios";
      });
      API.getListaUsuarios(1).then(function(result) {
        $scope.usuarios = result.data;
        $scope.num_pag = 1; //la pagina inicial es la 1
        //esta funcion actualiza los datos con pagina nueva
        $scope.page = function(numero) {
          var num_minimo = 1;
          //division entera
          var num_maximo = (result.total/result.count>>0);
          if(result.total%result.count>0) //si sobra algun elemento, crear una pagina adicional
            num_maximo+=1;
          //que el numero de pagina sea valido
          if(numero>num_maximo)
            numero=num_maximo;
          if(numero<num_minimo)
            numero=num_minimo;
          //llamada asincrona para cambiar de pagina
          API.getListaUsuarios(numero).then(
            function(resultados) { //callback success
              $scope.usuarios = resultados.data;
              $scope.num_pag = numero;
            },
            function(error) { //calback error
              cuadro_mensaje("Fallo de la base de datos o el servidor",true);
            });
        }

        $scope.ver_perfil = function(user) {
          $.mobile.pageContainer.pagecontainer(
            "change", "#perfilAjenoPage",{usuario: user});
        }

      });
    }

    //cada vez que se entre en la pagina de usuarios, se recarga
    $('#listaUsuariosPage').on('pagebeforeshow', function() {
      $scope.recargarLista();
    });

  }]
);


//Controlador para el perfil de usuario ajeno
appControllers.controller('perfilAjenoCtrl',['$scope','API',
  function ($scope,API) {
    $scope.usuario={};
    //conseguir el objeto que le hemos pasado con el pagecontainer
    $(document).on("pagebeforechange", function (e, data) {
      if (data.toPage[0].id == "perfilAjenoPage") {
          var usuario = data.options.usuario;
          if(usuario!=undefined)
          {
            $scope.usuario=usuario;
          }
      }
    });
  }]
);


//Controlador para ver,modificar y borrar perfil
appControllers.controller('miPerfilCtrl',['$scope','$rootScope','API',
  function ($scope,$rootScope,API) {
    $scope.usuario = {};
    $scope.inicializarControlador = function() {
      //El titulo (que sale en la pestaña y eso)
      $(document).ready(function ()
      {
        document.title = "Mi perfil de usuario";
      });
       API.getUsuario(
        JSON.parse(localStorage.getItem('user')).id)
        .then(function(result) {
          $scope.usuario.nombre = result.data[0].nombre;

          $scope.usuario.genero = result.data[0].genero;

          //apagamos todos los radio button
          $("#radio-choice-1" ).prop("checked",false).checkboxradio("refresh");
          $("#radio-choice-2" ).prop("checked",false).checkboxradio("refresh");
          $("#radio-choice-1" ).prop("checked",false).checkboxradio("refresh");

          //y encendemos el que sea
          if($scope.usuario.genero=="hombre")
            $("#radio-choice-1").prop("checked",true ).checkboxradio("refresh");
          else if($scope.usuario.genero=="mujer")
            $("#radio-choice-2").prop("checked",true ).checkboxradio("refresh");
          else
            $("#radio-choice-3").prop("checked",true ).checkboxradio("refresh");


          $scope.usuario.email = result.data[0].email;
          $scope.usuario.telefono = result.data[0].telefono;

          $scope.modificar_perfil = function() {
            var nuevos_datos = {};
            if($scope.usuario.nombre!=undefined && $scope.usuario.nombre!="")
            {
              nuevos_datos.nuevo_nombre = $scope.usuario.nombre;
              nuevos_datos.nuevo_genero = $scope.usuario.genero;
              nuevos_datos.nuevo_email = $scope.usuario.email;
              nuevos_datos.nuevo_telefono = $scope.usuario.telefono;
              API.modificarUsuarioLogeado(nuevos_datos)
              .then(function() {
                //actualizara el header si se ha cambiado el nombre
                $rootScope.recargarHeader();
                //mensaje confirmacion
                cuadro_mensaje("¡Perfil modificado!",false);
                //recargar
                $scope.inicializarControlador();
              });
            }
            else
            {
              cuadro_mensaje("El campo de nombre no puede estar vacío...",true);
            }
          }


          $scope.borrar_perfil = function() {
            API.borrarUsuarioLogeado()
            .then(function(result) {
              $.mobile.pageContainer.pagecontainer(
                "change", "#loginpage");
            });
          }
      });

    };

    //cada vez que se entre en la pagina de mi perfil, recargar
    $('#miPerfilPage').on('pagebeforeshow', function() {
      $scope.inicializarControlador();
    });
  }]
);
