// ========
module.exports = {
	login: function (req,res,next) {
	  	//el nombre de usuario es usuario, el password es password
	  	//esta la cadena usuario:password en base64
	  	//tiene la palabra Basic para ser compatible con postman
	  	//PROBLEMA: en postman, cuando aciertas con las credenciales,
	  	//la proxima vez aunque las pongas mal te deja porque las ha guardado.
	  	//cuando haya problemas, cerrar y abrir postman
	    var secret = 'Basic dXN1YXJpbzpwYXNzd29yZA==';
	    var auth = req.get('authorization');
		//var auth = null;
		if(auth==null) {
			return res
				.status(401)
				.set('WWW-Authenticate','Basic realm="Mi realm"')
				.send("No has introducido tus credenciales");
	  	}
	  	if(auth!=secret) {
    		return res.status(403).send("Las credenciales introducidas son incorrectas");
  		}
  		return next();
	},
	isEmpty: function (obj) {
	    for(var prop in obj) 
	    {
			if(obj.hasOwnProperty(prop))
	        	return false;
	    }
	    return true;
  	}
};



