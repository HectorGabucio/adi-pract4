"use strict";

module.exports = function(sequelize, DataTypes) {
  var Servicio = sequelize.define("Servicio", {
    titulo: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        //un servicio es creado por un solo usuario
        Servicio.belongsTo(models.Usuario, {
          onDelete: "CASCADE",
          constraints : false,
          foreignKey: {allowNull: false}
        });

        //un servicio es contratado por muchos usuarios
        //Servicio.hasMany(models.Usuario, {through: 'Contratos'});

      }
    }
  });

  return Servicio;
};
