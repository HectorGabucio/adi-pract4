var app = require('../app');
var supertest = require('supertest');
var assert = require('assert');

//para ejecutar los test, usa el comando "npm test" (sin comillas) en la raiz del proyecto, a la altura de package.json
describe('modificar-servicio-usuario-tests', function(){
	it('modificar servicio usuario devuelve 204', function(done) {
  	supertest(app)
    	.put("/api/usuarios/1/servicios/5")
    	.set('Authorization','Basic dXN1YXJpbzpwYXNzd29yZA==')
    	.send('titulo=servicio_test_modificado')
    	.expect(204)
    	.end(done);

	});

  it('modificar servicio usuario con ids no numericos devuelve 400', function(done) {
    supertest(app)
      .put("/api/usuarios/aa/servicios/5")
      .set('Authorization','Basic dXN1YXJpbzpwYXNzd29yZA==')
      .send('titulo=servicio_test_modificado')
      .expect(function(respuesta) {
        assert(respuesta.text.indexOf("Los id deben ser numéricos.")!=-1);
      })
      .expect(400)
      .end(done);

  });

  it('modificar servicio de usuario no existente devuelve 404', function(done) {
    supertest(app)
      .put("/api/usuarios/998/servicios/17")
      .set('Authorization','Basic dXN1YXJpbzpwYXNzd29yZA==')
      .send('titulo=servicio_test_modificado')
      .expect(function(respuesta) {
        assert(respuesta.text.indexOf("No existe un usuario con dicha id.")!=-1);
      })
      .expect(404)
      .end(done);

  });


  it('modificar servicio no existente para ese usuario devuelve 404', function(done) {
    supertest(app)
      .put("/api/usuarios/1/servicios/17")
      .set('Authorization','Basic dXN1YXJpbzpwYXNzd29yZA==')
      .send('titulo=servicio_test_modificado')
      .expect(function(respuesta) {
        assert(respuesta.text.indexOf("No existe ese servicio para dicho usuario")!=-1);
      })
      .expect(404)
      .end(done);

  });

  it('modificar servicio usuario sin body titulo devuelve 400', function(done) {
    supertest(app)
      .put("/api/usuarios/2/servicios/17")
      .set('Authorization','Basic dXN1YXJpbzpwYXNzd29yZA==')
      .expect(function(respuesta) {
        assert(respuesta.text.indexOf("Falta el campo titulo o su valor está vacío.")!=-1);
      })
      .expect(400)
      .end(done);

  });


})