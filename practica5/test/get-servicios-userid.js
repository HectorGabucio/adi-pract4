var app = require('../app');
var supertest = require('supertest');
var assert = require('assert');


//para ejecutar los test, usa el comando "npm test" (sin comillas) en la raiz del proyecto, a la altura de package.json
describe('get servicios-usuario-id-tests', function(){
	it('get servicios de usuario devuelve servicios', function(done) {
		supertest(app)
		.get('/api/usuarios/1/servicios')
		.set('Authorization','Basic dXN1YXJpbzpwYXNzd29yZA==')
		.expect(200)
		.expect(function(respuesta) {
			assert(respuesta.text.indexOf('{"_links":{"self":{"href":"/api/usuarios/1/servicios"},"first":{"href":"/api/usuarios/1/servicios"},"prev":{"href":"/api/usuarios/1/servicios"},')!=-1);
		})
		.end(done);
	});

	it('get servicios de usuario con id no numerico devuelve 400', function(done) {
		supertest(app)
		.get('/api/usuarios/abcde/servicios')
		.set('Authorization','Basic dXN1YXJpbzpwYXNzd29yZA==')
		.expect(400)
		.expect(function(respuesta) {
			assert(respuesta.text.indexOf("El id debe ser numérico.")!=-1);
		})
		.end(done);
	});

	it('get servicios de usuario con id no existente devuelve 404', function(done) {
		supertest(app)
		.get('/api/usuarios/-1/servicios')
		.set('Authorization','Basic dXN1YXJpbzpwYXNzd29yZA==')
		.expect(404)
		.expect(function(respuesta) {
			assert(respuesta.text.indexOf("El recurso con dicho id no existe.")!=-1);
		})
		.end(done);
	});

	it('get servicios de usuario con pagina incorrecta devuelve 400', function(done) {
		supertest(app)
		.get('/api/usuarios/1/servicios?page=-1')
		.set('Authorization','Basic dXN1YXJpbzpwYXNzd29yZA==')
		.expect(400)
		.expect(function(respuesta) {
			assert(respuesta.text.indexOf("Número de página no válido")!=-1);
		})
		.end(done);
	});

})
