var app = require('../app');
var supertest = require('supertest');
var assert = require('assert');

//para ejecutar los test, usa el comando "npm test" (sin comillas) en la raiz del proyecto, a la altura de package.json
describe('get usuario-id-tests', function(){
	it('get usuario por id devuelve usuario por id', function(done) {
		supertest(app)
		.get('/api/usuarios/1')
		.set('Authorization','Basic dXN1YXJpbzpwYXNzd29yZA==')
		.expect(200)
		.expect(function(respuesta) {
			assert(respuesta.text.indexOf('[{"id":1')!=-1);
		})
		.end(done);
	});

	it('get usuario con id no numerico devuelve 400', function(done) {
		supertest(app)
		.get('/api/usuarios/abcde')
		.set('Authorization','Basic dXN1YXJpbzpwYXNzd29yZA==')
		.expect(400)
		.expect(function(respuesta) {
			assert(respuesta.text.indexOf("El id debe ser numérico.")!=-1);
		})
		.end(done);
	});


	it('get usuario con id no existente devuelve 404', function(done) {
		supertest(app)
		.get('/api/usuarios/-1')
		.set('Authorization','Basic dXN1YXJpbzpwYXNzd29yZA==')
		.expect(404)
		.expect(function(respuesta) {
			assert(respuesta.text.indexOf("El recurso con dicho id no existe.")!=-1);
		})
		.end(done);
	});

})