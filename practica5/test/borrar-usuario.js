var app = require('../app');
var supertest = require('supertest');
var assert = require('assert');

//para ejecutar los test, usa el comando "npm test" (sin comillas) en la raiz del proyecto, a la altura de package.json
describe('borrar-usuario-tests', function(){
  this.timeout(15000);
	it('borrar usuario devuelve 204', function(done) {
    	supertest(app)
      	.delete("/api/usuarios/4")
      	.set('Authorization','Basic dXN1YXJpbzpwYXNzd29yZA==')
      	.expect(204)
        .end(done);
	});

  it('borrar usuario id no numerico devuelve 400', function(done) {
      supertest(app)
        .delete("/api/usuarios/hola")
        .set('Authorization','Basic dXN1YXJpbzpwYXNzd29yZA==')
        .expect(400)
        .expect(function(respuesta) {
          assert(respuesta.text.indexOf("El id debe ser numérico.")!=-1);
        })
        .end(done);

  });

  it('borrar usuario id no existente devuelve 404', function(done) {
      supertest(app)
        .delete("/api/usuarios/9998")
        .set('Authorization','Basic dXN1YXJpbzpwYXNzd29yZA==')
        .expect(404)
        .expect(function(respuesta) {
          assert(respuesta.text.indexOf("El recurso con dicho id no existe.")!=-1);
        })
        .end(done);

  });




})