var app = require('../app');
var supertest = require('supertest');
var assert = require('assert');

//para ejecutar los test, usa el comando "npm test" (sin comillas) en la raiz del proyecto, a la altura de package.json
describe('borrar-servicio-usuario-tests', function(){
  this.timeout(15000);
	it('borrar servicio de usuario devuelve 204', function(done) {
    	supertest(app)
      	.delete("/api/usuarios/1/servicios/1")
      	.set('Authorization','Basic dXN1YXJpbzpwYXNzd29yZA==')
      	.expect(204)
        .end(done);
	});

  it('borrar servicio de usuario con ids no numericos devuelve 400', function(done) {
      supertest(app)
        .delete("/api/usuarios/a/servicios/b")
        .set('Authorization','Basic dXN1YXJpbzpwYXNzd29yZA==')
        .expect(400)
        .expect(function(respuesta) {
          assert(respuesta.text.indexOf("Los id deben ser numéricos.")!=-1);
        })
        .end(done);
  });

  it('borrar servicio de usuario no existente devuelve 404', function(done) {
      supertest(app)
        .delete("/api/usuarios/998/servicios/1")
        .set('Authorization','Basic dXN1YXJpbzpwYXNzd29yZA==')
        .expect(404)
        .expect(function(respuesta) {
          assert(respuesta.text.indexOf("No existe un usuario con dicha id.")!=-1);
        })
        .end(done);
  });

  it('borrar servicio no existente de ese usuario devuelve 404', function(done) {
      supertest(app)
        .delete("/api/usuarios/1/servicios/15")
        .set('Authorization','Basic dXN1YXJpbzpwYXNzd29yZA==')
        .expect(404)
        .expect(function(respuesta) {
          assert(respuesta.text.indexOf("No existe ese servicio para dicho usuario")!=-1);
        })
        .end(done);
  });



})