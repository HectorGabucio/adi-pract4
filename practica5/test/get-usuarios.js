var app = require('../app');
var supertest = require('supertest');
var assert = require('assert');

//para ejecutar los test, usa el comando "npm test" (sin comillas) en la raiz del proyecto, a la altura de package.json
describe('get usuarios-tests', function(){
	it('get usuarios devuelve los usuarios', function(done) {
		supertest(app)
		.get('/api/usuarios')
		.set('Authorization','Basic dXN1YXJpbzpwYXNzd29yZA==')
		.expect(200)
		.expect(function(respuesta) {
			assert(respuesta.text.indexOf('{"_links":{"self":{"href":"/api/usuarios"}')!=-1);
		})
		.end(done);
	});

	it('get usuarios con credenciales malas devuelve 403',function(done) {
		supertest(app)
		.get('/api/usuarios')
		.set('Authorization','Basic dXN1YXJpb')
		.expect(403)
		.expect(function(respuesta) {
			assert(respuesta.text.indexOf("Las credenciales introducidas son incorrectas")!=-1);
		})
		.end(done);
	});

	it('get usuarios con credenciales inexistentes devuelve 401',function(done) {
		supertest(app)
		.get('/api/usuarios')
		.expect(401)
		.expect('WWW-Authenticate','Basic realm="Mi realm"')
		.expect(function(respuesta) {
			assert(respuesta.text.indexOf("No has introducido tus credenciales")!=-1);
		})
		.end(done);
	});

	it('get usuarios con pagina no valida devuelve 400',function(done) {
		supertest(app)
		.get('/api/usuarios?page=-1')
		.set('Authorization','Basic dXN1YXJpbzpwYXNzd29yZA==')
		.expect(400)
		.expect(function(respuesta) {
			assert(respuesta.text.indexOf("Número de página no válido")!=-1);
		})
		.end(done);
	});


})