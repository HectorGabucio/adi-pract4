var MiApp = angular.module('miApp', [
  'ui.router',
  'appControllers',
  'appServices',
  'ui.bootstrap'
]);


//activamos html5 para que no salgan las molestas # en las rutas
MiApp.config(["$locationProvider", function($locationProvider) {
	$locationProvider.html5Mode({
		enabled: true,
		requireBase: false
	});
}]);

//VERSION UI-ROUTER
MiApp.config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise("servicios");

    $stateProvider
    //el estado PADRE, tendra globales como headers footers o sidebars
    .state('root',{
      url: '',
      abstract: true,
      views: {
        'header': {
          templateUrl: 'aplicacion/templates/header.html',
          controller: 'HeaderCtrl'
        }
      }
    })
    //la lista de servicios
    .state('servicios', {
      parent: "root",
      url: "/servicios",
      views: {
        'container@': {
          templateUrl: "/aplicacion/templates/service-list.html",
          controller: 'ServiceListCtrl',
        }
      },
      params: {
        mensaje:null
      },
      data: {
		    requireLogin: true
		  }
    })
    //ver mis servicios
    .state('mis_servicios', {
      parent: "root",
      url: "/mis_servicios",
      views: {
        'container@': {
          templateUrl: "/aplicacion/templates/service-list.html",
          controller: 'MisServiciosCtrl',
        }
      }, 
      params: {
        mensaje:null
      },
      data: {
        requireLogin: true
      }
    })
    .state('mi_perfil', {
      parent: "root",
      url: "/mi_perfil",
      views: {
        'container@': {
          templateUrl: "/aplicacion/templates/mi_perfil.html",
          controller: "PerfilCtrl"
        }
      },
      params: {
        mensaje:null
      },
      data: {
        requireLogin: true
      }
    })
    .state('perfil_ajeno', {
      parent: "root",
      url: "/perfil_usuario/:user_id",
      views: {
        'container@': {
          templateUrl: "/aplicacion/templates/mi_perfil.html",
          controller: "PerfilAjenoCtrl"
        }
      },
      data: {
        requireLogin: true
      }
    })
    .state('usuarios', {
      parent: "root",
      url: "/usuarios",
      views: {
        'container@': {
          templateUrl: "/aplicacion/templates/usuarios.html"
          //controller: "VerUsuariosCtrl"
        }
      },
      params: {
        mensaje:null
      },
      data: {
        requireLogin: true
      }
    })
    .state('crear_usuario', {
      parent: "root",
      url: "/registrarse",
      views: {
        'container@': {
          templateUrl: "/aplicacion/templates/crear_usuario.html"
          //controller: 'CrearUsuarioCtrl'
        }
      },
      data: {
        requireLogin: false
      }
    })
    .state('nologin', {
      parent: "root",
    	url: "/",
    	views: {
        'container@': {
          templateUrl: 'aplicacion/templates/login-modal.html',
          controller: 'LoginCtrl'
        }
      },
      params: {
        mensaje:null
      },
    	data: {
			requireLogin: false
			}
    });

    //localStorage.clear(); //AL REFRESCAR LA PAGINA, EMPIEZA TODO DE 0
  });

//evento, al cambiar/refrescar ruta), miramos si necesita login.
MiApp.run(function ($rootScope,$state, loginModal) {
	$rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
    	var requireLogin = toState.data.requireLogin;
      if(requireLogin && !localStorage.getItem('user')) { 
        event.preventDefault();
        // se dispara el evento si el user no esta logeado
        // ejecutamos la funcion que devolvia el servicio
        loginModal()
        .then(function () { //si consigue logearse, pasa al estado objetivo
          $state.go(toState.name,
          {mensaje: "Has iniciado sesión correctamente"});
        })
        .catch(function () { //si cierra la ventana...
          return $state.go('nologin');
        });
    	}
  	});
});