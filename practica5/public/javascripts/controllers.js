var appControllers = angular.module('appControllers', ['appServices']);

//javascript para lanzar el mensaje
var cuadro_mensaje = function(mensaje,esError) {
  var id;
  var decorado;
  var clase;

  if(esError) {
    id="error-message";
    decorado = '<i class="glyphicon glyphicon-alert"></i> ';
    clase="alert alert-danger notificacion";
  }

  else {
    id="success-message";
    decorado = '<i class="glyphicon glyphicon-ok"></i> ';
    clase="alert alert-success notificacion";
  }

  var elem = document.getElementById(id);
  //le ponemos un mensaje
  elem.innerHTML = decorado + mensaje;

  //alert y alert-danger es para hacerlo bonito con bootstrap
  //notificacion es una clase css que arranca la transicion
  elem.className = clase;

}

//con esta funcion, el cuadro podra salir mas veces
var reiniciar_cuadro = function() {
  if(document.getElementById("error-message")!=null)
  {
    var elem = document.getElementById("error-message");
    elem.className = "";
    elem.innerHTML="";
  }
  if(document.getElementById("success-message")!=null)
  {
    var elem = document.getElementById("success-message");
    elem.className = "";
    elem.innerHTML="";
  }
}


//controlador para los datos del header
appControllers.controller('HeaderCtrl',['$scope','$document','$rootScope','$state',
  function($scope,$document,$rootScope,$state) {
    $scope.titulo = 'No has iniciado sesión todavía';
    $scope.enlace_cerrar_sesion=true;
    if(localStorage.getItem('user'))
    {
        $scope.titulo = "Bienvenido " + JSON.parse(localStorage.getItem('user')).login;
    }


    var encender = function(seleccionado) {
      //Jquery para los botones de la barra.
      //encendemos en el estado que estemos
      $(document).ready(function () {
        $('#servicios').removeClass('active');
        $('#mis_servicios').removeClass('active');
        $('#mi_perfil').removeClass('active');
        $('#usuarios').removeClass('active');
        $('#nologin').removeClass('active');

        seleccionado = '#' + seleccionado;
        //encendemos el elegido
        $(seleccionado).addClass('active');
      });
    }
    

    //cuando cambia el estado del login, actualizar barra header
    $scope.$watch('logeado',function(newValue){
      if($rootScope.logeado)
      {
        $scope.titulo = "Bienvenido " + JSON.parse(localStorage.getItem('user')).login;
        $scope.enlace_cerrar_sesion=true;
      }
      else
      {
        $scope.titulo = 'No has iniciado sesión todavía';
        $scope.enlace_cerrar_sesion=false;
      }
    });

    //para saber si se ha actualizado el nombre del logeado desde el perfil
    $scope.$watch('actualizar_nombre',function() {
      $scope.titulo = "Bienvenido " + JSON.parse(localStorage.getItem('user')).login;
    });

    //para encender o marcar en la header el estado donde esta
    $scope.$watch('encender',function() {
      var estado = $rootScope.encender;
      encender(estado);

    })

    $scope.logout = function() {
      localStorage.clear();
      return $state.go('nologin',
          {mensaje: "Has cerrado la sesión"});
    };
    $scope.home = function() {

      return $state.go('servicios');
    };

    $scope.crear_servicio = function() {
      return $state.go('crear_servicio');
    };

    $scope.mis_servicios = function() {
      return $state.go('mis_servicios');
    };

    $scope.mi_perfil = function() {
      return $state.go('mi_perfil');
    }
    $scope.usuarios = function() {
      return $state.go('usuarios');
    }
  }]
);

//controlador para la lista de servicios
appControllers.controller('ServiceListCtrl', ['$scope','$state','$stateParams','API',
  function ($scope,$state,$stateParams,API) {
    reiniciar_cuadro();
    if($stateParams.mensaje)
      cuadro_mensaje($stateParams.mensaje,false); //mostramos el mensaje recibido de otro estado
    //esto solo se ejecutara una vez
    //esta la callback de success y la de error
    API.getListaServicios(1).then(function(result) {
      $scope.puedo_modificar=false;
      $scope.services = result.data;
      $scope.num_pag = 1; //la pagina inicial es la 1
      //esta funcion actualiza los datos con pagina nueva
      $scope.page = function(numero) {
        reiniciar_cuadro();
        var num_minimo = 1;
        //division entera
        var num_maximo = (result.total/result.count>>0);
        if(result.total%result.count>0) //si sobra algun elemento, crear una pagina adicional
          num_maximo+=1;
        //que el numero de pagina sea valido
        if(numero>num_maximo)
          numero=num_maximo;
        if(numero<num_minimo)
          numero=num_minimo;
        //llamada asincrona para cambiar de pagina
        API.getListaServicios(numero).then(
          function(resultados) { //callback success
            $scope.services = resultados.data;
            $scope.num_pag = numero;
          },
          function(error) { //calback error
            cuadro_mensaje("Fallo de la base de datos o el servidor",true);
          });
      }
    });
  }]
);

//controlador para ver MIS SERVICIOS
appControllers.controller('MisServiciosCtrl', ['$scope','$state','$stateParams','API',
  function ($scope,$state,$stateParams,API) {
    reiniciar_cuadro();
    if($stateParams.mensaje)
      cuadro_mensaje($stateParams.mensaje,false); //mostramos el mensaje recibido de otro estado
    $scope.puedo_modificar=true;
    $scope.creando=false;
    $scope.activeServiceIndex; //nos servira para saber que servicio estamos modificando
    $scope.datos = {}; //inicializamos datos de modificado
    $scope.datos_crear = {}; //inicializamos datos de nuevo servicio

    var actualizar = function(num_pag) {
      $scope.num_pag = num_pag; //la pagina inicial es la 1
      API.getMisServicios($scope.num_pag).then(function(result) {
        $scope.services = result.data;
        //esta funcion actualiza los datos con pagina nueva
        $scope.page = function(numero) {
          reiniciar_cuadro();
          $scope.activeServiceIndex=-1; //no tiene que haber ningun form desplegado
          var num_minimo = 1;
          //division entera
          var num_maximo = (result.total/result.count>>0);
          if(result.total%result.count>0) //si sobra algun elemento, crear una pagina adicional
            num_maximo+=1;
          //que el numero de pagina sea valido
          if(numero>num_maximo)
            numero=num_maximo;
          if(numero<num_minimo)
            numero=num_minimo;
          //llamada asincrona para cambiar de pagina
          API.getMisServicios(numero).then(
            function(resultados) { //callback success
              $scope.services = resultados.data;
              $scope.num_pag = numero;
            },
            function(error) { //calback error
              cuadro_mensaje("Fallo de la base de datos o el servidor",true);
            }
          );
        };
      });
    }

    //ejecutamos la primera vez para mostrar datos
    actualizar(1);


    $scope.borrar = function(servicio_id) {
      reiniciar_cuadro();
      API.borrarServicio(servicio_id).then(function(result) {
        //actualizamos el borrado en la lista
        actualizar($scope.num_pag);
        cuadro_mensaje("Se ha borrado el servicio satisfactoriamente",false);
      })
    };

    $scope.crear_servicio = function() {
      reiniciar_cuadro();
      //necesito la id de ese usuario
      var user_id = JSON.parse(localStorage.getItem('user')).id;
      //consigo el objeto user por su login
        $scope.datos_crear.usuario = user_id;
        API.crearServicio($scope.datos_crear)
        .then(function(result) {
          $scope.creando=false; //cerramos el form de creacion
          actualizar($scope.num_pag);
          cuadro_mensaje("Se ha creado el servicio " + result.data.titulo,false)
        })
        .catch(function(error) {
          cuadro_mensaje(error.data,true);
        });

    }

    $scope.modificar_servicio = function(service_id) {
      reiniciar_cuadro();
      API.modificarServicio(service_id,$scope.datos.nuevo_titulo)
      .then(function(result) {
        //cerramos form de modificado
        $scope.activeServiceIndex = -1;
        //actualizamos el modificado en la lista
        actualizar($scope.num_pag);
        cuadro_mensaje("Servicio modificado correctamente",false);
      })
      .catch(function(error) {
        cuadro_mensaje(error.data,true);
      })
    };

    //esconde y muestra form de creacion de servicio
    $scope.form_crear = function() {
      //limpiar input
      $scope.datos_crear = {};
      if($scope.creando)
      {
        $scope.creando=false;
      }
      else
      {
        $scope.creando=true;
      }
    }

    $scope.form_editar = function(indice_servicio) {
      //limpiar input
      $scope.datos = {};

      //ocultar el form 
      if($scope.activeServiceIndex===indice_servicio)
      {
        $scope.activeServiceIndex = -1;
      }
      else
        //muestra el form
        $scope.activeServiceIndex = indice_servicio;
    }

    $scope.editando = function(index){
        return  $scope.activeServiceIndex === index;
    };

  }]
);

//controlador para el perfil

//CUIDADO
//
//
//si modificamos el NOMBRE del usuario, tambien tenemos que modificar
//el nombre en el LOCALSTORAGE
appControllers.controller('PerfilCtrl', ['$scope','$rootScope','$state','$stateParams','API','loginSvc',
  function ($scope,$rootScope,$state,$stateParams,API,loginSvc) {
    reiniciar_cuadro();
    if($stateParams.mensaje)
      cuadro_mensaje($stateParams.mensaje,false); //mostramos el mensaje recibido de otro estado
    $scope.modificando = false; //al principio, la vista es no modificable
    $scope.usuario = {};
    $scope.datos = {};
    var actualizar = function() {
      $scope.modificando = false; //al principio, la vista es no modificable
      $scope.usuario = {};
      API.getUsuario(JSON.parse(localStorage.getItem('user')).id).then(function(result) {
        $scope.usuario.nombre = result.data[0].nombre;
        $scope.usuario.genero = result.data[0].genero;
        $scope.usuario.email = result.data[0].email;
        $scope.usuario.telefono = result.data[0].telefono;
      });
    }
    actualizar();

    $scope.dar_de_baja = function() {
      API.borrarUsuarioLogeado().then(function(result) {
        $state.go('nologin',
          {mensaje: "Has dado de baja tu cuenta"});
      })
    }

    $scope.form_modificar = function() {
      //limpiar input
      $scope.datos={};

      if($scope.modificando===false)
        $scope.modificando = true;
      else
        $scope.modificando = false;
    }

    $scope.modificar_perfil = function() {
      reiniciar_cuadro();
      API.modificarUsuarioLogeado($scope.datos)
      .then(function(result) {
        $rootScope.actualizar_nombre++; //esto disparara el WATCH del header, y actualizara el header
        actualizar();
        cuadro_mensaje("Perfil modificado correctamente",false);
      })
      .catch(function(error) {
        cuadro_mensaje(error.data,true);
      })
    }
  }]
);


//controlador para el PERFIL DE OTRO USUARIO
//no podemos editar ni borrar este perfil
//recibe el id del usuario en $stateParams.user_id

appControllers.controller('PerfilAjenoCtrl', ['$scope','$state','$stateParams','API',
  function ($scope,$state,$stateParams,API) {
    var user_id=$stateParams.user_id;
    $scope.usuario = {};
    $scope.visitando=true; //visitando, asi que escondemos los botones de modificar y borrar

    API.getUsuario(user_id).then(function(result) {
      $scope.usuario.nombre = result.data[0].nombre;
      $scope.usuario.genero = result.data[0].genero;
      $scope.usuario.email = result.data[0].email;
      $scope.usuario.telefono = result.data[0].telefono;
    });

    $scope.atras = function() {
      $state.go('usuarios');
    }
  }]
);


//ESTE ES EL LOGIN MODAL
appControllers.controller('LoginModalCtrl',['$scope','API', 
  function ($scope,API) {
    //scope tiene dismiss, ya que es un modal
    $scope.cancel = $scope.$dismiss;

    //esconder el boton de registrarse
    $scope.esconder_register=true;
    $scope.submit = function (username, password) {
      reiniciar_cuadro();
      API.login(username, password).then(
        function(result) { //callback resultado
          //scope tiene close ya que es un modal
          //se devuelve el usuario que es correcto
          var user ={};
          user.id=result.data[0].id;
          user.login=username;
          user.password=password;
          //aqui mandamos al service los datos del usuario
          $scope.$close(user);
        },
        function(error) { //callback error
          cuadro_mensaje(error.data,true);
          //cerramos modal por error
          $scope.cancel();
        });
    };
  }]
);


//ESTE ES EL LOGIN QUE NO ES MODAL
appControllers.controller('LoginCtrl',['$scope','$state','$stateParams','API','loginSvc',
  function ($scope,$state,$stateParams,API,loginSvc) {
    reiniciar_cuadro();
    if($stateParams.mensaje)
      cuadro_mensaje($stateParams.mensaje,false); //mostramos el mensaje recibido de otro estado
    if(localStorage.getItem('user')) //si ya estamos logeados, redirigir
    {
      $state.go('servicios');
    }

    $scope.esconder_register=false;

    $scope.submit = function (username, password) {
      reiniciar_cuadro(); //notificacion preparada para salir si hay error
      API.login(username, password).then(
        function(result) { //callback resultado
          //scope tiene close ya que es un modal
          //se devuelve el usuario que es correcto
          var user ={};
          user.id=result.data[0].id;
          user.login=username;
          user.password=password;
          //aqui mandamos al service los datos del usuario
          loginSvc(user);
          $state.go('servicios',
            {mensaje: "El usuario " + user.login + " ha iniciado sesión."});
        },
        function(error) { //callback error
          cuadro_mensaje(error.data,true);
        });
    };
    $scope.register = function() {
      return $state.go('crear_usuario');
    };
  }]
);


//evento, al cambiar/refrescar ruta),
//refrescamos estado de header mirando el login
//y mirando a que estado quiere ir
appControllers.run(function ($rootScope,$state) {
  $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
    //avisar al header de que se cambia de estado
    $rootScope.encender = toState.name;

    //saber si sigue logeado o no
    if(localStorage.getItem('user'))
      $rootScope.logeado=true;
    else
      $rootScope.logeado=false;
  });
});
