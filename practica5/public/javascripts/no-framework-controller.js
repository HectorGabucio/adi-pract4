
var getCredenciales = function() {
      var credenciales = "INCORRECTAS";
      if(localStorage.getItem('user')) {
        credenciales = JSON.parse(localStorage.getItem('user'));

        //Esto lo hago porque en la API, las credenciales son siempre usuario:password
        //en la aplicacion web necesito guardar en el localstorage un nombre de usuario
        //existente en la BD (Paco, Héctor...)
        credenciales.login='usuario';
        credenciales = credenciales.login + ':' + credenciales.password;
        //aqui ya tenemos la cadena en basic64 preparada
        credenciales = 'Basic ' + btoa(credenciales);
      }
      return credenciales;
    };


var anterior = function() {
  var num = document.getElementById('num_pagina').innerHTML;
  if(num>1) 
    num--;
  getListaUsuarios(num);
}

var siguiente = function() {
  var num = document.getElementById('num_pagina').innerHTML;
  num++;
  var num_max = document.getElementById('num_pagina_maxima').innerHTML;
  if(num>num_max)
    num=num_max;
  getListaUsuarios(num);
}


var getListaUsuarios = function(num_pagina) {
	var url = '/api/usuarios';
    if(num_pagina>1)
      url = url + '?page=' + num_pagina;
	$.ajax({
		url: url,
		async: true,
		type: 'GET',
		beforeSend: function (request)
        {
        	request.setRequestHeader("Authorization", getCredenciales());
        },
		success: function(results) {
      //actualizamos num pagina
      document.getElementById('num_pagina').innerHTML=num_pagina;
      var num_maximo = (results.total/results.count>>0);
      if(results.total%results.count>0) //si sobra algun elemento, crear una pagina adicional
        num_maximo+=1;
      document.getElementById('num_pagina_maxima').innerHTML=num_maximo;

      document.getElementById("filas").innerHTML=""; //vaciamos filas
			var i=1;
			results.data.forEach(function(entry) {
        var nombre= entry.nombre;
        var enlace = "/perfil_usuario/" + entry.id;
        var plantilla = '<div class="row">' +
        '<div class="col-sm-4">' + nombre + '</div>' +
        '<div class="col-sm-4">' +
        '<a href=' + enlace + ' class="btn btn-primary btn-sm">' +
        '<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>' +
        '</a>' +
        '</div>'+
        '<div class="col-sm-4">☆☆☆☆☆</div>'+
        '</div>';

        document.getElementById("filas").innerHTML+=plantilla;

			});
 		}
	});
 };

 //javascript para lanzar el mensaje
var cuadro_mensaje = function(mensaje,esError) {
  var id;
  var decorado;
  var clase;

  if(esError) {
    id="error-message";
    decorado = '<i class="glyphicon glyphicon-alert"></i> ';
    clase="alert alert-danger notificacion";
  }

  else {
    id="success-message";
    decorado = '<i class="glyphicon glyphicon-ok"></i> ';
    clase="alert alert-success notificacion";
  }

  var elem = document.getElementById(id);
  //le ponemos un mensaje
  elem.innerHTML = decorado + mensaje;

  //alert y alert-danger es para hacerlo bonito con bootstrap
  //notificacion es una clase css que arranca la transicion
  elem.className = clase;

}

//con esta funcion, el cuadro podra salir mas veces
var reiniciar_cuadro = function() {
  if(document.getElementById("error-message")!=null)
  {
    var elem = document.getElementById("error-message");
    elem.className = "";
    elem.innerHTML="";
  }
  if(document.getElementById("success-message")!=null)
  {
    var elem = document.getElementById("success-message");
    elem.className = "";
    elem.innerHTML="";
  }
}


 var crearUsuario = function() {
  reiniciar_cuadro();
  var data = {};
  data.nombre = document.getElementById("nombre").value;
  data.genero = $('input[name=genero]:checked').val();
  data.email = document.getElementById("email").value;
  data.telefono = document.getElementById("telefono").value;
  $.ajax({
    url: '/api/usuarios/',
    data: data,
    async: true,
    type: 'POST',
    success: function(results) {
      cuadro_mensaje("Te has registrado correctamente.",false);
      //guardamos en localstorage
      var user ={};
      user.id=results.id;
      user.login=results.nombre;
      user.password='password';
      localStorage.setItem('user', JSON.stringify(user));
      window.location.replace('servicios');
    },
    error: function(results) {
      cuadro_mensaje(results.responseText,true);
    }
  });
 };

$( document ).ready(function() {
  reiniciar_cuadro();
  //comportamiento para la pagina de registro de usuario
	if(document.getElementById("btn-registrar"))
    document.getElementById("btn-registrar").addEventListener("click",crearUsuario);

  //comportamiento para la pagina de usuarios mas valorados
  if(document.getElementById("contenido"))
  {
    //escondemos nuestros contadores
    document.getElementById("num_pagina").style.visibility = "hidden";
    document.getElementById("num_pagina_maxima").style.visibility = "hidden";

    //hacemos la llamada primera
    getListaUsuarios(document.getElementById('num_pagina').innerHTML);
    //añadimos los listeners de paginado
    document.getElementById("anterior").addEventListener("click",anterior);
    document.getElementById("siguiente").addEventListener("click",siguiente);
  }

});