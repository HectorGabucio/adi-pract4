

"use strict";

module.exports = function(sequelize, DataTypes) {
  var Usuario = sequelize.define("Usuario", {
    nombre: {
      type: DataTypes.STRING,
      unique: true
    },
    genero: DataTypes.STRING,
    email: DataTypes.STRING,
    telefono: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        //un usuario puede crear muchos servicios
        Usuario.hasMany(models.Servicio);
        //un usuario contrata muchos servicios
        //Usuario.hasMany(models.Servicio, {through: 'Contratos'});
      }
    }
  });


  return Usuario;
};
