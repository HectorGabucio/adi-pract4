var express = require('express');
var router  = express.Router();
var utils = require('../utils/utils.js');
var models  = require('../models');

//caso de uso, login
router.post('/',function(req, res) {
  if(utils.isEmpty(req.body.login) || utils.isEmpty(req.body.password)) {
    return res.status(400).send("Son necesarios los parámetros login y password");
  }


  //si las credenciales son correctas...
  var where = { where: {nombre: req.body.login } };
  models.Usuario.findAll(where)
  .then(function(usuario) {
      if(utils.isEmpty(usuario) || req.body.password!='password')
      {
        res.status(403).send("Credenciales incorrectas");
      } else 
      {
        res.status(200).send(usuario);
      }
  });
});


module.exports = router;