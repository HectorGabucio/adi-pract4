var models  = require('../models');
var express = require('express');
var router  = express.Router();
var hal = require('hal');
var utils = require('../utils/utils.js'); //definido el login y isEmpty

//estamos en /api/usuarios
//key para crear/modificar nombre de usuarios-> nombre
//key para crear servicio -> titulo



//caso de uso, listado de todos los usuarios
//solo devuelve los usuarios, sin sus tareas
//la idea es que en una web al clickar al usuario, aparezca la pagina de usuario
router.get('/',utils.login,function(req,res) {
  models.Usuario.findAll()
  .then(function(usuarios) {
    //el total de usuarios
    var total = usuarios.length;
    //si el total es 0, no hay paginacion ni nada
    if(total==0) {
      return res.status(200).send(usuarios);
    }
    //en cada pagina tendremos 5 objetos
    var page_size = 5;

    var num_paginas = (total/page_size>>0); //division entera
    var resto =  total % page_size;

    if(resto>0) //si sobran items sin paginar, se añade una ultima pagina para ellos
      num_paginas++;

    var pagina = '1'; //si no se ha definido el num de pag, por defecto es la pagina 1
    if(req.query.page!=undefined) 
    {
      pagina=req.query.page;
    }

    var entero_pag = parseInt(pagina);
    if(entero_pag<1 || entero_pag>num_paginas || isNaN(entero_pag)) //paginacion no valida
    {
      return res.status(400).send("Número de página no válido");
    }

    var link_actual = '/api/usuarios';
    if(pagina!='1') //si no es la primera, añadimos query string de page
      link_actual = 'api/usuarios?page=' + pagina;

    var link_first = '/api/usuarios'; //siempre es la misma
    var link_prev = '/api/usuarios';
    if(pagina!='1' && pagina!='2') //si no es la primera ni la segunda, tendra un query string en prev
      link_prev = '/api/usuarios?page=' + (entero_pag-1);

    var link_next = '/api/usuarios?page=' + num_paginas;
    if(entero_pag<num_paginas) //si es menor que el total de paginas, su next sera pagina+1
      link_next = '/api/usuarios?page=' + (entero_pag+1);

    var link_last = '/api/usuarios?page=' + num_paginas; //siempre es la misma
    if(num_paginas==1) //si solo hay 1 pag, el next y el last son igual que el first
    {
      link_next = link_first;
      link_last = link_first;
    }

    //creando objeto json + paginacion
    var resource = new hal.Resource({},link_actual); //aqui añadimos el link self
    resource.link('first',link_first); //añadimos los demas links
    resource.link('prev',link_prev);
    resource.link('next',link_next);
    resource.link('last',link_last);

    resource.count = page_size; //añadimos el count
    resource.total = total;   //añadimos el total

    var data = [];

    //configuramos indices de inicio y final
    var it_inicio = (entero_pag-1)*page_size;
    var it_fin = (entero_pag*page_size);
    if(it_fin>total)  //no puede pasarse del total
      it_fin=total;

    for (var i = it_inicio; i < it_fin; i++) { 
      data.push(usuarios[i]);
    }
    resource.data = data; //añadimos el objeto data con los 5 objetos que toquen
    res.status(200).send(resource); //mandamos el objeto json
    //res.status(200).send(usuarios);
  });
})




//caso de uso, get usuario por su id
//solo muestra el usuario que apunta la id,
//sera como un filtro en la pagina de busqueda de usuarios
//no es necesario cargar los servicios, ya que eso lo hacemos
//en /api/usuarios/:user_id/servicios/
router.get('/:user_id',utils.login,function(req, res) {
  if(isNaN(req.params.user_id))
  {
    return res.status(400).send("El id debe ser numérico.");
  }
  var where = { where: {id: req.params.user_id } };
  models.Usuario.findAll(where)
  .then(function(usuario) {
      if(utils.isEmpty(usuario))
      {
        res.status(404).send("El recurso con dicho id no existe.");
      } else
      {
        res.status(200).send(usuario);
      }
  });
});



//caso de uso, listado de servicios de un usuario
//se mostraran los servicios de ese usuario,
//no se cargara el objeto de usuario ya que es redundante.
//es mas, se omite el atributo UsuarioId, porque ya lo sabemos, esta en la ruta
router.get('/:user_id/servicios',utils.login, function(req, res) {
  var user_id = req.params.user_id;
  if(isNaN(user_id))
  {
    return res.status(400).send("El id debe ser numérico.").end();
  }
  var where = { where: {id: user_id} };
  return models.Usuario.findAll(where)
  .then(function(usuario) {
    if(utils.isEmpty(usuario))
    {
      return res.status(404).send("El recurso con dicho id no existe.");
    } else //si que existe el user
    {
      //omitimos el UsuarioID porque ya lo sabemos con la ruta
      return models.Servicio.findAll({
        where: {UsuarioId: user_id },
        attributes: ['id','titulo','createdAt','updatedAt']
      })
      .then(function(servicios) {
        //res.status(200).send(servicios);
        //el total de servicios
        var total = servicios.length;

        //si el total es 0, no hay paginacion ni nada
        if(total==0) {
          return res.status(200).send(servicios);
        }
        //en cada pagina tendremos 5 objetos
        var page_size = 5;

        var num_paginas = (total/page_size>>0); //division entera
        var resto =  total % page_size;

        if(resto>0) //si sobran items sin paginar, se añade una ultima pagina para ellos
          num_paginas++;

        var pagina = '1'; //si no se ha definido el num de pag, por defecto es la pagina 1
        if(req.query.page!=undefined) 
        {
          pagina=req.query.page;
        }

        var entero_pag = parseInt(pagina);
        if(entero_pag<1 || entero_pag>num_paginas || isNaN(entero_pag)) //paginacion no valida
        {
          return res.status(400).send("Número de página no válido");
        }
        var base = '/api/usuarios/' + user_id;
        var link_actual = base + '/servicios';
        if(pagina!='1') //si no es la primera, añadimos query string de page
          link_actual = base + '/servicios?page=' + pagina;

        var link_first = base + '/servicios'; //siempre es la misma
        var link_prev = base + '/servicios';
        if(pagina!='1' && pagina!='2') //si no es la primera ni la segunda, tendra un query string en prev
          link_prev = base + '/servicios?page=' + (entero_pag-1);

        var link_next = base + '/servicios?page=' + num_paginas;
        if(parseInt(pagina)<num_paginas) //si es menor que el total de paginas, su next sera pagina+1
          link_next = base + '/servicios?page=' + (entero_pag+1);

        var link_last = base + '/servicios?page=' + num_paginas; //siempre es la misma
        if(num_paginas==1) //si solo hay 1 pag, el next y el last son igual que el first
        {
          link_next = link_first;
          link_last = link_first;
        }

        //creando objeto json + paginacion
        var resource = new hal.Resource({},link_actual); //aqui añadimos el link self
        resource.link('first',link_first); //añadimos los demas links
        resource.link('prev',link_prev);
        resource.link('next',link_next);
        resource.link('last',link_last);

        resource.count = page_size; //añadimos el count
        resource.total = total;   //añadimos el total

        var data = [];

        //configuramos indices de inicio y final
        var it_inicio = (entero_pag-1)*page_size;
        var it_fin = (entero_pag*page_size);
        if(it_fin>total)  //no puede pasarse del total
          it_fin=total;

        for (var i = it_inicio; i < it_fin; i++) { 
            data.push(servicios[i]);
        }
        resource.data = data; //añadimos el objeto data con los 5 objetos que toquen
        return res.status(200).send(resource); //mandamos el objeto json
      });
    }
  })
});







//caso de uso, crear un nuevo usuario
//no va a necesitar autorizacion, ya que será para registrar un nuevo usuario
router.post('/',function(req, res) {
  if(utils.isEmpty(req.body.nombre)) {
    return res.status(400).send("Falta el campo nombre, o su valor está vacío.");
  }

  models.Usuario.create({
    nombre: req.body.nombre,
    genero: req.body.genero, //estos 3 siguientes campos no son obligatorios
    email: req.body.email,
    telefono: req.body.telefono
  }).then(function(usuario) {
    res.location('/api/usuarios/' + usuario.id);
    res.status(201).send(usuario);
  }).catch(function(e) {
    res.status(400).send("Ese nombre de usuario ya está en uso");
  })
});





//caso de uso, crear un nuevo servicio para un usuario concreto
router.post('/:user_id/servicios',utils.login, function (req, res) {
  if(isNaN(req.params.user_id))
  {
    return res.status(400).send("El id debe ser numérico.").end();
  }
  else if(utils.isEmpty(req.body.titulo)) {
    return res.status(400).send("Falta el campo titulo, o su valor está vacío.").end();
  }

  else {
    var where = { where: {id: req.params.user_id } };
    models.Usuario.findAll(where)
    .then(function(usuario) {
        if(utils.isEmpty(usuario))
        {
          return res.status(404).send("El recurso con dicho id no existe.").end();
        } else //si que existe el user
        {
          return models.Servicio.create({
            titulo: req.body.titulo,
            UsuarioId: req.params.user_id})
          .then(function(servicio) {
            return res.location('/api/usuarios/' + req.params.user_id + '/servicios/' + servicio.id)
            .status(201)
            .send(servicio)
            .end();
          });
        }
    })
  }
});







//caso de uso, modificar usuario
router.put('/:user_id',utils.login,function(req, res) {
  if(isNaN(req.params.user_id))
  {
    return res.status(400).send("El id debe ser numérico.");
  }
  var where = { where: {id: req.params.user_id } };
  models.Usuario.findAll(where)
  .then(function(usuario) {
      if(utils.isEmpty(usuario))
      {
        return res.status(404).send("El recurso con dicho id no existe.");
      } else //si que existe el user
      {
        if(utils.isEmpty(req.body.nombre))
        {
          return res.status(400).send("Falta el campo nombre, o su valor está vacío.");
        }
        var genero = req.body.genero; //no es obligatorio
        var email = req.body.email; //no es obligatorio
        var telefono = req.body.telefono; //no es obligatorio;
        var values = { 
          nombre: req.body.nombre,
          genero: genero,
          email: email,
          telefono: telefono
         };
        var selector = { where: { id: req.params.user_id } };
        return models.Usuario.update(values,selector)
      }
  })
  .then(function() {
    return res.status(204).end();
  });
});



//caso de uso, borrar un usuario
router.delete('/:user_id',utils.login,function(req, res) {
  if(isNaN(req.params.user_id))
  {
    return res.status(400).send("El id debe ser numérico.");
  }
  var where = { where: {id: req.params.user_id } };
  models.Usuario.findAll(where)
  .then(function(usuario) {
      if(utils.isEmpty(usuario))
      {
        return res.status(404).send("El recurso con dicho id no existe.");
      } else //si que existe el user
      {
        return models.Usuario.destroy({
          where: {
            id: req.params.user_id
          }
        });
      }
  })
  .then(function() {
    res.status(204).end();
  });
});






//caso de uso, borrar un servicio de un usuario determinado 
router.delete('/:user_id/servicios/:service_id',utils.login,function (req, res) {
  if(isNaN(req.params.user_id) || isNaN(req.params.service_id))
  {
    return res.status(400).send("Los id deben ser numéricos.");
  }
  //COMPROBAR QUE EXISTE USER
  var where = { where: {id: req.params.user_id } };
  models.Usuario.findAll(where)
  .then(function(usuario) {
      if(utils.isEmpty(usuario))
      {
        return res.status(404).send("No existe un usuario con dicha id.");
      } else //si que existe el user
      {
        var where = { where: {UsuarioId: req.params.user_id,
                              id: req.params.service_id } };
        return models.Servicio.findAll(where)
      }
  }) //comprobar que existe el servicio
  .then(function(servicio) {
      if(utils.isEmpty(servicio)) {
        return res.status(404).send("No existe ese servicio para dicho usuario");
      }
      return models.Servicio.destroy({
        where: {
          id: req.params.service_id
        }
      });
  }).then(function() {
      return res.status(204).end();
  });  
});








//caso de uso, modificar un servicio de un usuario determinado
router.put('/:user_id/servicios/:service_id',utils.login,function (req, res) {
  if(isNaN(req.params.user_id) || isNaN(req.params.service_id))
  {
    return res.status(400).send("Los id deben ser numéricos.");
  }
  //COMPROBAR QUE EXISTE USER
  var where = { where: {id: req.params.user_id } };
  models.Usuario.findAll(where)
  .then(function(usuario) {
      if(utils.isEmpty(usuario))
      {
        return res.status(404).send("No existe un usuario con dicha id.");
      } else //si que existe el user
      {
        var where = { where: {UsuarioId: req.params.user_id,
                              id: req.params.service_id } };
        return models.Servicio.findAll(where)
      }
  }) //comprobar que existe el servicio
  .then(function(servicio) {
      if(utils.isEmpty(servicio)) {
        return res.status(404).send("No existe ese servicio para dicho usuario");
      }
      if(utils.isEmpty(req.body.titulo))
      {
        return res.status(400).send("Falta el campo titulo o su valor está vacío.");
      }
      var values = { titulo: req.body.titulo };
      var selector = { where: { id: req.params.service_id } };
      return models.Servicio.update(values,selector);
  }).then(function() {
      return res.status(204).end();
  });  
});


module.exports = router;